#!/usr/bin/env python

def main():
	print "Welcome to the AIMS module"
		
def std(list_x):      # list_x as the list of numbers
	n = len(list_x)
	if n == 0:        # checks if the list is empty
		return 0.0
	x_bar = sum(list_x)/float(len(list_x))
	sum_dev_sq = 0.0
	for i in range(len(list_x)):
		sum_dev_sq = sum_dev_sq + (list_x[i]-x_bar)**2			
	sigma = (sum_dev_sq/float(len(list_x)))**0.5
	return sigma
	
def avg_range(x):    # average range function and user enters a file as a list
    n = len(x)
    if n == 0:
        return 0.0
    if type(x) != list:
        return "Input is not a list"            
    myfile = open(x[0] , 'r')
    for line in myfile:    
        line = line.strip()
        if line.startswith('Range'):                  
            line_list = line.split(':')
            return int(line_list[1])	

	
if __name__=='__main__':
	main()
