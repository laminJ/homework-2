import aims
from nose.tools import assert_equal, assert_almost_equal, assert_true,\
assert_false, assert_raises, assert_is_instance

def test_std_int():       # testing integers
    obs = aims.std([ 1 , 3])
    exp = 1.0
    assert_equal(obs, exp)

def test_std_empt():       # zero division test
    obs = aims.std([])
    exp = 0.0
    assert_equal(obs, exp)

def test_std_neg():        # testing negative numbers
    obs = aims.std([-1,-3,-5])
    exp = 1.63299316
    assert_almost_equal(obs, exp)

def test_std_flt():        # testing floats 
    obs = aims.std([1.0, 2.0, 3.0, 4.0, 5.0])
    exp = 1.41421356
    assert_almost_equal(obs, exp)
    
# testing the average range function    

def test_ave_range_empt():  # zero division test
    obs = aims.avg_range([])
    exp = 0.0
    assert_equal(obs, exp)


def test_ave_range_nonstr():  # testing another file in the same directory
    obs = aims.avg_range(['data/bert/audioresult-00330'])
    exp = 4.0
    assert_equal(obs, exp)


def test_ave_range_str():  # type test
    obs = aims.avg_range('data/bert/audioresult-00215')
    exp = "Input is not a list"
    assert_equal(obs, exp)
